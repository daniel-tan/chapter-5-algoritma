function findClosestTarget(arr) {
	// you can only write your code here!
     let jarak = 0;
     let cek;
     for (let i = 0; i < arr.length; i++) {
          if ((arr[i] == "o") || (arr[i] == "x")) {
               if (arr[i] == "o") {
                    for (let j = i; j < arr.length; j++) {
                         if (arr[j] == "x") {
                              cek = j - i;
                              if (jarak == 0) {
                                   jarak = cek;
                              } else if (jarak > cek) {
                                   jarak = cek;
                              }
                              break;
                         }
                    }
               } else {
                    for (let j = i; j < arr.length; j++) {
                         if (arr[j] == "o") {
                              cek = j - i;
                              if (jarak == 0) {
                                   jarak = cek;
                              } else if (jarak > cek) {
                                   jarak = cek;
                              }
                              break;
                         }
                    }
               }
               
          }
     }

     return jarak;
}

// TEST CASES
console.log(findClosestTarget([" ", " ", "o", " ", " ", "x", " ", "x"])); // 3

console.log(findClosestTarget(["o", " ", " ", " ", "x", "x", "x"])); // 4
console.log(findClosestTarget(["x", " ", " ", " ", "x", "x", "o", " "])); // 1
console.log(findClosestTarget([" ", " ", "o", " "])); // 0
console.log(findClosestTarget([" ", "o", " ", "x", "x", " ", " ", "x"])); // 2

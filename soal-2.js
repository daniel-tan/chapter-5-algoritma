function groupingAnimalBasedOnHurufDepan(animals) {
     const newAnimals = [];
     
     // you can only write your code here! 
     for (let i = 0; i < animals.length; i++) {
          let tampung = [];
          // let k = i;
          for (let j = i; j < animals.length; j++) {
               console.log("i dan j = ",animals[i], animals[j]);
               if (animals[i] === animals[j]) {
                    continue;
               } else if (animals[i].charAt(0) == animals[j].charAt(0)) {             
                    tampung.push(animals[j], animals[i]);
                    console.log("tampung", tampung);
               }

               if (j == animals.length-1) {                    
                    if (tampung.length === 0)
                    {
                         newAnimals.push(animals[i]);
                    } else {
                         newAnimals.push(tampung);
                    }
               }
          }
     }

     return newAnimals;
}


// TEST CASES
console.log(groupingAnimalBasedOnHurufDepan  (['cacing', 'ayam', 'kuda', 'anoa', 'kancil']));
// [ ['ayam', 'anoa'], ['cacing'], ['kuda', 'kancil'] ]

console.log(groupingAnimalBasedOnHurufDepan  (['cacing', 'ayam', 'kuda', 'anoa', 'kancil', 'unta', 'cicak']));
// [ ['ayam', 'anoa'], ['cacing', 'cicak'], ['kuda', 'kancil'], ['unta'] ]
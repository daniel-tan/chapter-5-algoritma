function findGeometryLogic(arr){ 
  	// you can only write your code here!
	let ratio;
	let nextRatio;
	for (let i = 1; i < arr.length-1; i++) {
		ratio = arr[i] / arr[i-1];
		nextRatio = arr[i+1] / arr[i];
		if (ratio != nextRatio) {
			return false;
		}
	}
	return true;
}

// TEST CASES
console.log(findGeometryLogic([1, 3, 9, 27, 81])); // true
console.log(findGeometryLogic([2, 4, 8, 16, 32])); // true
console.log(findGeometryLogic([2, 4, 6, 8])); // false
console.log(findGeometryLogic([2, 6, 18, 54])); // true
console.log(findGeometryLogic([1, 2, 3, 4, 7, 9])); // false